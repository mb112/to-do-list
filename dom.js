const addTask = document.getElementById("addTask")
const taskList = document.getElementById("taskList")
const textArea = document.getElementById('taskInput')


addTask.addEventListener("submit",function (event) {
    event.preventDefault()

    let removeButton = document.createElement("button")
    removeButton.setAttribute("type","button")
    removeButton.innerText="Done"
    removeButton.classList.add("done")
    let input = document.getElementById("taskInput").value
    let newElement = document.createElement("li")
    newElement.classList.add("task")
    newElement.innerText=input
    taskList.append(newElement)
    newElement.append(removeButton)
    textArea.value = null;

    removeButton.addEventListener("click", function(event) {
        event.target.parentElement.remove();
    })
})

